class TripItemsModel {
  final int totalCount;
  final List<TripItemModel> resultList;
  TripItemsModel({this.totalCount, this.resultList});

  factory TripItemsModel.fromJson(Map<String, dynamic> json) {
    if (json == null) {
      return null;
    }

    List<TripItemModel> list = List<TripItemModel>();
    (json['resultList'] as List).forEach((json) {
      list.add(TripItemModel.fromJson(json));
    });
    return TripItemsModel(totalCount: json['totalCount'], resultList: list);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> map = Map<String, dynamic>();
    map['totalCount'] = totalCount;
    if (resultList != null) {
      map['resultList'] = resultList
          .map((TripItemModel itemModel) => itemModel.toJson())
          .toList();
    }
    return map;
  }
}

class TripItemModel {
  final int type;
  final ArticleModel article;
  TripItemModel({this.type, this.article});

  factory TripItemModel.fromJson(Map<String, dynamic> json) {
    return json != null
        ? TripItemModel(
            type: json['type'], article: ArticleModel.fromJson(json['article']))
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> map = Map<String, dynamic>();
    map['type'] = type;
    if (article != null) {
      map['article'] = article.toJson();
    }
    return map;
  }
}

class ArticleModel {
  /// 文章id
  final int articleId;

  /// 文章标题
  final String articleTitle;

  /// 点赞数
  final int likeCount;

  /// 用户模型
  final AuthorModel author;

  /// 图片模型数组
  final List<ImageModel> images;

  /// 位置信息
  final List<PositionModel> positions;

  final List<URLModel> urls;

  ArticleModel(
      {this.articleId,
      this.articleTitle,
      this.likeCount,
      this.author,
      this.images,
      this.positions,
      this.urls});

  factory ArticleModel.fromJson(Map<String, dynamic> json) {
    if (json == null) {
      return null;
    }

    List<ImageModel> imagesList = List<ImageModel>();
    if (json['images'] != null) {
      (json['images'] as List).forEach((json) {
        imagesList.add(ImageModel.fromJson(json));
      });
    }

    List<PositionModel> positionList = List<PositionModel>();
    if (json['pois'] != null) {
      (json['pois'] as List).forEach((json) {
        positionList.add(PositionModel.fromJson(json));
      });
    }

    List<URLModel> urlList = List<URLModel>();
    if (json['urls'] != null) {
      (json['urls'] as List).forEach((json) {
        urlList.add(URLModel.fromJson(json));
      });
    }

    return ArticleModel(
        articleId: json['articleId'],
        articleTitle: json['articleTitle'],
        likeCount: json['likeCount'],
        author: AuthorModel.fromJson(json['author']),
        images: imagesList,
        positions: positionList,
        urls: urlList);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> map = Map<String, dynamic>();
    map['articleId'] = articleId;
    map['articleTitle'] = articleTitle;
    map['likeCount'] = likeCount;
    if (author != null) {
      map['author'] = author.toJson();
    }
    return map;
  }
}

class AuthorModel {
  /// 用户id
  final int authorId;

  /// 昵称
  final String nickName;

  /// 个人主页
  final String jumpUrl;

  /// 头像
  final String dynamicUrl;
  final String originalUrl;
  AuthorModel(
      {this.authorId,
      this.nickName,
      this.jumpUrl,
      this.dynamicUrl,
      this.originalUrl});

  factory AuthorModel.fromJson(Map<String, dynamic> json) {
    return json != null
        ? AuthorModel(
            authorId: json['authorId'],
            nickName: json['nickName'],
            jumpUrl: json['jumpUrl'],
            dynamicUrl: (json['coverImage'] as Map)['dynamicUrl'],
            originalUrl: (json['coverImage'] as Map)['originalUrl'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> map = Map<String, dynamic>();
    map['authorId'] = authorId;
    map['nickName'] = nickName;
    map['jumpUrl'] = jumpUrl;
    map['dynamicUrl'] = dynamicUrl;
    map['originalUrl'] = originalUrl;
    return map;
  }
}

class ImageModel {
  final String dynamicUrl;
  final String originalUrl;
  final double width;
  final double height;
  ImageModel({this.dynamicUrl, this.originalUrl, this.width, this.height});

  factory ImageModel.fromJson(Map<String, dynamic> json) {
    return json != null
        ? ImageModel(
            dynamicUrl: json['dynamicUrl'],
            originalUrl: json['originalUrl'],
            width: json['width'],
            height: json['height'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> map = Map<String, dynamic>();
    map['dynamicUrl'] = dynamicUrl;
    map['originalUrl'] = originalUrl;
    map['width'] = width;
    map['height'] = height;
    return map;
  }
}

class PositionModel {
  final String districtName;
  final String districtENName;
  final String countryName;
  PositionModel({this.districtName, this.districtENName, this.countryName});

  factory PositionModel.fromJson(Map<String, dynamic> json) {
    return json != null
        ? PositionModel(
            districtName: json['districtName'],
            districtENName: json['districtENName'],
            countryName: json['countryName'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> map = Map<String, dynamic>();
    map['districtName'] = districtName;
    map['districtENName'] = districtENName;
    map['countryName'] = countryName;
    return map;
  }
}

class URLModel {
  final String appUrl;
  final String h5Url;
  final String wxUrl;
  URLModel({this.appUrl, this.h5Url, this.wxUrl});

  factory URLModel.fromJson(Map<String, dynamic> json) {
    return json != null
        ? URLModel(
            appUrl: json['appUrl'],
            h5Url: json['h5Url'],
            wxUrl: json['wxUrl'],
          )
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> map = Map<String, dynamic>();
    map['appUrl'] = appUrl;
    map['h5Url'] = h5Url;
    map['wxUrl'] = wxUrl;
    return map;
  }
}
