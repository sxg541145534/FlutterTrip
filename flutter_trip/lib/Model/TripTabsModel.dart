class TripTabsModel {
  final String url;
  final List<TripTabItemModel> tabs;
  final Map<String,dynamic> params;
  TripTabsModel({this.url, this.tabs,this.params});

  factory TripTabsModel.fromJson(Map<String, dynamic> json) {
    if (json == null) return null;

    List<TripTabItemModel> list = List<TripTabItemModel>();
    if (json['tabs'] != null) {
      (json['tabs'] as List).forEach((json) {
        list.add(TripTabItemModel.fromJson(json));
      });
    }

    return TripTabsModel(url: json['url'], tabs: list,params: json['params']);
  }

  Map<String,dynamic> toJson() {
    final Map<String, dynamic> map = Map<String, dynamic>();
    map['url'] = url;
    if (tabs != null) {
      List<Map> list = List<Map>();
      tabs.forEach((item){
        list.add(item.toJson());
      });
      map['tabs'] = list;
    }
    return map;
  }
}

class TripTabItemModel {
  final String labelName;
  final String groupChannelCode;
  TripTabItemModel({this.labelName, this.groupChannelCode});

  factory TripTabItemModel.fromJson(Map<String, dynamic> json) {
    return json != null
        ? TripTabItemModel(
            labelName: json['labelName'],
            groupChannelCode: json['groupChannelCode'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> map = Map<String, dynamic>();
    map['labelName'] = labelName;
    map['groupChannelCode'] = groupChannelCode;
    return map;
  }
}
