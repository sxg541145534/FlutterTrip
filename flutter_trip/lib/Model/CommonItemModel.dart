class CommonItemModel {
  /// 图标
  final String icon;
  /// 标题
  final String title;
  /// 地址
  final String url;
  /// 状态栏颜色
  final String statusBarColor;
  /// 导航栏状态
  final bool hideAppBar;

  CommonItemModel({this.icon, this.title, this.url, this.statusBarColor,
      this.hideAppBar});
  factory CommonItemModel.fromJson(Map<String,dynamic> json) {
    return json != null ? CommonItemModel(
      icon: json['icon'],
      title: json['title'],
      url: json['url'],
      statusBarColor: json['statusBarColor'],
      hideAppBar: json['hideAppBar'],
    ) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> map = Map<String, dynamic>();
    map['title'] = title;
    map['icon'] = icon;
    map['url'] = url;
    map['hideAppBar'] = hideAppBar;
    map['statusBarColor'] = statusBarColor;
    return map;
  }
}