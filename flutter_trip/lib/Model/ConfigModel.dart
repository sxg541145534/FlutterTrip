class ConfigModel {
  /// 搜索地址
  final String searchUrl;

  ConfigModel({this.searchUrl});
  factory ConfigModel.fromJson(Map<String,dynamic> json) {
    return json != null ? ConfigModel(
      searchUrl: json['searchUrl']
    ) : null;
  }

  Map<String,dynamic> toJson() {
    final Map<String,dynamic> map = Map<String,dynamic>();
    map['searchUrl'] = searchUrl;
    return map;
  }
}