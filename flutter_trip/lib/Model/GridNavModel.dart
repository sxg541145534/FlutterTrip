import 'CommonItemModel.dart';

class GridNavModel {
  final GridNavItemModel hotel;
  final GridNavItemModel flight;
  final GridNavItemModel travel;

  GridNavModel({this.hotel, this.flight, this.travel});
  factory GridNavModel.fromJson(Map<String,dynamic> json) {
    return json != null ? GridNavModel(
      hotel: GridNavItemModel.fromJson(json['hotel']),
      flight: GridNavItemModel.fromJson(json['flight']),
      travel: GridNavItemModel.fromJson(json['travel']),
    ) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> map = Map<String, dynamic>();
    if (hotel != null) {
      map['hotel'] = hotel.toJson();
    }
    if (flight != null) {
      map['flight'] = flight.toJson();
    }
    if (travel != null) {
      map['travel'] = travel.toJson();
    }
    return map;
  }
}


class GridNavItemModel {
  final String startColor;
  final String endColor;
  final CommonItemModel mainItem;
  final CommonItemModel item1;
  final CommonItemModel item2;
  final CommonItemModel item3;
  final CommonItemModel item4;

  GridNavItemModel({this.startColor, this.endColor, this.mainItem, this.item1, this.item2, this.item3, this.item4});
  factory GridNavItemModel.fromJson(Map<String,dynamic> json) {
    return json != null ? GridNavItemModel(
      startColor: json['startColor'],
      endColor: json['endColor'],
      mainItem: CommonItemModel.fromJson(json['mainItem']),
      item1: CommonItemModel.fromJson(json['item1']),
      item2: CommonItemModel.fromJson(json['item2']),
      item3: CommonItemModel.fromJson(json['item3']),
      item4: CommonItemModel.fromJson(json['item4']),
    ) : null;
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> map = Map<String, dynamic>();
    map['startColor'] = startColor;
    map['endColor'] = endColor;
    if (mainItem != null) {
      map['mainItem'] = mainItem.toJson();
    }

    if (item1 != null) {
      map['item1'] = item1.toJson();
    }

    if (item2 != null) {
      map['item2'] = item2.toJson();
    }

    if (item3 != null) {
      map['item3'] = item3.toJson();
    }

    if (item4 != null) {
      map['item4'] = item4.toJson();
    }
    return map;
  }
}