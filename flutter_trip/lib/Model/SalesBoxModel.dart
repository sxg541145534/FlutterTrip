import 'CommonItemModel.dart';

class SalesBoxModel {
  final String icon;
  final String moreUrl;
  CommonItemModel bigCard1;
  CommonItemModel bigCard2;
  CommonItemModel smallCard1;
  CommonItemModel smallCard2;
  CommonItemModel smallCard3;
  CommonItemModel smallCard4;

  SalesBoxModel({this.icon, this.moreUrl, this.bigCard1, this.bigCard2,
      this.smallCard1, this.smallCard2, this.smallCard3, this.smallCard4});
  factory SalesBoxModel.fromJson(Map<String,dynamic> json) {
    return json != null ? SalesBoxModel(
      icon: json['icon'],
      moreUrl: json['moreUrl'],
      bigCard1: CommonItemModel.fromJson(json['bigCard1']),
      bigCard2: CommonItemModel.fromJson(json['bigCard2']),
      smallCard1: CommonItemModel.fromJson(json['smallCard1']),
      smallCard2: CommonItemModel.fromJson(json['smallCard2']),
      smallCard3: CommonItemModel.fromJson(json['smallCard3']),
      smallCard4: CommonItemModel.fromJson(json['smallCard4']),
    ) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> map = Map<String, dynamic>();
    map['icon'] = icon;
    map['moreUrl'] = moreUrl;
    if (bigCard1 != null) {
      map['bigCard1'] = bigCard1.toJson();
    }
    if (bigCard2 != null) {
      map['bigCard2'] = bigCard2.toJson();
    }
    if (smallCard1 != null) {
      map['smallCard1'] = smallCard1.toJson();
    }
    if (smallCard2 != null) {
      map['smallCard2'] = smallCard2.toJson();
    }
    if (smallCard3 != null) {
      map['smallCard3'] = smallCard3.toJson();
    }
    if (smallCard4 != null) {
      map['smallCard4'] = smallCard4.toJson();
    }
    
    return map;
  }
}