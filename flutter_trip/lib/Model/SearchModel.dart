class SearchModel {
  final List<SearchItemModel> data;
  final String keyword;
  SearchModel({this.data, this.keyword});

  factory SearchModel.fromJson(Map<String, dynamic> json) {
    List list = (json['data'] as List)
        .map((json) => SearchItemModel.fromJson(json))
        .toList();
    return json != null
        ? SearchModel(data: list, keyword: json['keyword'])
        : null;
  }

  List<Map<dynamic, dynamic>> toJson() {
    List<Map<dynamic, dynamic>> list = List<Map<dynamic, dynamic>>();
    data.forEach((SearchItemModel itemModel) {
      list.add(itemModel.toJson());
    });
    return list;
  }
}

class SearchItemModel {
  final String word;
  final String type;
  final String price;
  final String star;
  final String districtname;
  final String url;
  final String zonename;
  SearchItemModel(
      {this.word,
      this.type,
      this.price,
      this.star,
      this.districtname,
      this.url,
      this.zonename});

  factory SearchItemModel.fromJson(Map<String, dynamic> json) {
    return json != null
        ? SearchItemModel(
            word: json['word'],
            type: json['type'],
            price: json['price'],
            star: json['star'],
            districtname: json['districtname'],
            url: json['url'],
            zonename: json['zonename'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> map = Map<String, dynamic>();
    map['word'] = word;
    map['type'] = type;
    map['price'] = price;
    map['star'] = star;
    map['districtname'] = districtname;
    map['url'] = url;
    return map;
  }
}
