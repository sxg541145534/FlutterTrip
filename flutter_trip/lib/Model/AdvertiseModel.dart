class AdvertiseModel {
  final String url;
  AdvertiseModel({this.url});

  factory AdvertiseModel.fromJson(Map<String,dynamic> json) {
    return json != null ? AdvertiseModel(
      url: json['url']
    ) : null;
  }

  Map<String,dynamic> toJson() {
    final Map<String, dynamic> map = Map<String, dynamic>();
    map['url'] = url;
    return map;
  }
}