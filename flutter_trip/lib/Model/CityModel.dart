import 'package:azlistview/azlistview.dart';

class CityModel extends ISuspensionBean {
  /// 名称
  final String name;
  /// 拼音
  String pinyin;
  /// 字母索引
  String alphaIndex;
  CityModel({this.name});

  factory CityModel.fromJson(Map<String, dynamic> json) {
    return json != null ? CityModel(name: json['name']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> map = Map<String, dynamic>();
    map['name'] = name;
    return map;
  }

  @override
  String getSuspensionTag() => alphaIndex;
}
