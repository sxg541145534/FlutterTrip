import 'CommonItemModel.dart';
import 'ConfigModel.dart';
import 'GridNavModel.dart';
import 'SalesBoxModel.dart';

class HomeModel {
  final ConfigModel config;
  final List<CommonItemModel> bannerList;
  final List<CommonItemModel> localNavList;
  final GridNavModel gridNav;
  final List<CommonItemModel> subNavList;
  final SalesBoxModel salesBox;

  HomeModel({this.config, this.bannerList, this.localNavList, this.gridNav,
      this.subNavList, this.salesBox});
  factory HomeModel.fromJson(Map<String,dynamic> json) {
    if (json == null) {
      return null;
    }

    var bannerList;
    if (json['bannerList'] != null) {
      bannerList = (json['bannerList'] as List).map((json) {
        return CommonItemModel.fromJson(json);
      }).toList();
    }

    var localNavList;
    if (json['localNavList'] != null) {
      localNavList = (json['localNavList'] as List).map((json) {
        return CommonItemModel.fromJson(json);
      }).toList();
    }

    var subNavList;
    if (json['subNavList'] != null) {
      subNavList = (json['subNavList'] as List).map((json) {
        return CommonItemModel.fromJson(json);
      }).toList();
    }
    
    return  HomeModel(
      config: ConfigModel.fromJson(json['config']),
      bannerList: bannerList,
      localNavList: localNavList,
      gridNav: GridNavModel.fromJson(json['gridNav']),
      subNavList: subNavList,
      salesBox: SalesBoxModel.fromJson(json['salesBox'])
    );
  }

  Map<String,dynamic> toJson() {
    final Map<String, dynamic> map = Map<String, dynamic>();
    if (config != null) {
      map['config'] = config.toJson();
    }
    if (bannerList != null) {
      map['bannerList'] = bannerList.map((v) => v.toJson()).toList();
    }
    if (localNavList != null) {
      map['localNavList'] = localNavList.map((v) => v.toJson()).toList();
    }
    if (gridNav != null) {
      map['gridNav'] = gridNav.toJson();
    }
    if (subNavList != null) {
      map['subNavList'] = subNavList.map((v) => v.toJson()).toList();
    }
    if (salesBox != null) {
      map['salesBox'] = salesBox.toJson();
    }

    return map;
  }
}