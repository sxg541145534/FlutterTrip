import 'package:flutter/material.dart';
import 'package:flutter_trip/TabsPage/SearchPage.dart';
import 'package:flutter_trip/TabsPage/SpeakSearchPage.dart';
import 'package:flutter_trip/TabsPage/TabsPage.dart';
import 'package:flutter_trip/widget/WebViewWidget.dart';

//配置路由
final routes = {
  '/tabs': (context) => TabsPage(),
  '/webView':(context,{arguments}) => WebViewWidget(itemModel: arguments),
  '/search': (context) => SearchPage(isShowBackButton: true),
  '/speakSearch': (context) => SpeakSearchPage()
};

//固定写法
// ignore: top_level_function_literal_block
var routeGenerator = (RouteSettings settings) {
  // 统一处理
//  print(settings.arguments);
  final String name = settings.name;
  final Function pageContentBuilder = routes[name];
  if (pageContentBuilder != null) {
    if (settings.arguments != null) {
      final Route route = MaterialPageRoute(
          builder: (context) =>
              pageContentBuilder(context, arguments: settings.arguments));
      return route;
    } else {
      final Route route = MaterialPageRoute(
          builder: (context) =>
              pageContentBuilder(context));
      return route;
    }
  }
};