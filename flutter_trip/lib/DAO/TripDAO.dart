import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter_trip/Model/AdvertiseModel.dart';
import 'package:flutter_trip/Model/HomeModel.dart';
import 'package:flutter_trip/Model/SearchModel.dart';
import 'package:flutter_trip/Model/TripItemModel.dart';
import 'package:flutter_trip/Model/TripTabsModel.dart';
import 'package:http/http.dart' as http;

class TripDAO {
  static Future<HomeModel> loadHomeData() async {
    var response = await http
        .get('http://www.devio.org/io/flutter_app/json/home_page.json');
    if (response.statusCode == 200) {
      var jsonResponse = json.decode(utf8.decode(response.bodyBytes));
      return HomeModel.fromJson(jsonResponse);
    } else {
      print('请求首页接口失败');
      return null;
    }
  }

  static Future<SearchModel> loadSearchData({@required String keyword}) async {
    String url =
        'https://m.ctrip.com/restapi/h5api/searchapp/search?source=mobileweb&action=autocomplete&contentType=json&keyword=' +
            keyword;
    var response = await http.get(url);
    if (response.statusCode == 200) {
      Map<String, dynamic> jsonResponse =
          json.decode(utf8.decode(response.bodyBytes));
      jsonResponse['keyword'] = keyword;
      return SearchModel.fromJson(jsonResponse);
    } else {
      print('请求搜索数据失败');
      return null;
    }
  }

  static Future<AdvertiseModel> loadAdvertiseData() async {
    String url =
        'https://rich.kuwo.cn/AdService/kaiping/adinfo?version=kwplayer_ip_9.1.3.0&source=kwplayer_ip_9.1.3.0_TJ.ipa&width=750&height=1334&appuid=131572625';
    var response = await http.get(url);
    if (response.statusCode == 200) {
      var jsonResponse = json.decode(utf8.decode(response.bodyBytes));
      return AdvertiseModel.fromJson(jsonResponse);
    } else {
      print('请求搜索数据失败');
      return null;
    }
  }

  static Future<TripTabsModel> loadTripTabsData() async {
    final String url =
        'http://www.devio.org/io/flutter_app/json/travel_page.json';
    http.Response response = await http.get(url);
    if (response.statusCode == 200) {
      Map<String, dynamic> jsonResponse =
          json.decode(utf8.decode(response.bodyBytes));
      return TripTabsModel.fromJson(jsonResponse);
    } else {
      print('加载旅拍tabs分类数据失败');
      return null;
    }
  }

  static Future<TripItemsModel> loadTripItemData(
      {@required String groupChannelCode,
      @required int pageIndex,
      @required Map<String, dynamic> parameters}) async {
    final String url =
        'https://m.ctrip.com/restapi/soa2/16189/json/searchTripShootListForHomePageV2?_fxpcqlniredt=09031014111431397988&__gw_appid=99999999&__gw_ver=1.0&__gw_from=10650013707&__gw_platform=H5';
    parameters['pagePara']['pageIndex'] = pageIndex;
    parameters['groupChannelCode'] = groupChannelCode;
    http.Response response = await http.post(url, body: jsonEncode(parameters));
    if (response.statusCode == 200) {
      Map<String, dynamic> jsonResponse =
          json.decode(utf8.decode(response.bodyBytes));
      return TripItemsModel.fromJson(jsonResponse);
    } else {
      print('加载旅拍卡片列表失败');
      return null;
    }
  }
}
