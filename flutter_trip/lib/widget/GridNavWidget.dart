import 'package:flutter/material.dart';
import 'package:flutter_trip/Model/CommonItemModel.dart';
import 'package:flutter_trip/Model/GridNavModel.dart';

class GridNavWidget extends StatelessWidget {
  final GridNavModel gridNav;
  GridNavWidget({@required this.gridNav});

  @override
  Widget build(BuildContext context) {
    return Card(
        elevation: 2.0,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        margin: EdgeInsets.fromLTRB(4, 6, 4, 0),
        child: PhysicalModel(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10),
            clipBehavior: Clip.antiAlias,
            child: Column(children: _gridNavRows(context))));
  }

  List<Widget> _gridNavRows(BuildContext context) {
    if (gridNav == null) return [];

    List<Widget> rows = List<Widget>();
    if (gridNav.hotel != null) {
      rows.add(_gridNavRow(context: context, itemModel: gridNav.hotel));
    }
    if (gridNav.flight != null) {
      rows.add(_gridNavRow(
          context: context, itemModel: gridNav.flight, isCenter: true));
    }
    if (gridNav.travel != null) {
      rows.add(_gridNavRow(context: context, itemModel: gridNav.travel));
    }
    return rows;
  }

  Widget _gridNavRow(
      {@required BuildContext context,
      @required GridNavItemModel itemModel,
      isCenter = false}) {
    Color startColor = Color(int.parse('0xff' + itemModel.startColor));
    Color endColor = Color(int.parse('0xff' + itemModel.endColor));
    BorderSide borderSide = BorderSide(color: Colors.white, width: 1.0);
    Border border =
        isCenter ? Border(top: borderSide, bottom: borderSide) : null;
    return Container(
        height: 88.0,
        decoration: BoxDecoration(
            border: border,
            gradient: LinearGradient(colors: [startColor, endColor])),
        child: Row(children: <Widget>[
          Expanded(
              child: _mainItemWidget(
                  context: context, itemModel: itemModel.mainItem)),
          Expanded(
              child: _itemGroupWidget(
                  context: context,
                  itemModel1: itemModel.item1,
                  itemModel2: itemModel.item2)),
          Expanded(
              child: _itemGroupWidget(
                  context: context,
                  itemModel1: itemModel.item3,
                  itemModel2: itemModel.item4))
        ]));
  }

  Widget _mainItemWidget(
      {@required BuildContext context, @required CommonItemModel itemModel}) {
    return GestureDetector(
        onTap: () =>
            Navigator.of(context).pushNamed('/webView', arguments: itemModel),
        child: Stack(children: <Widget>[
          Align(
              alignment: Alignment.bottomCenter,
              child: Image.network(itemModel.icon)),
          Align(
              alignment: Alignment(0, -0.7),
              child: Text(itemModel.title,
                  style: TextStyle(color: Colors.white, fontSize: 14)))
        ]));
  }

  Widget _itemGroupWidget(
      {@required BuildContext context,
      @required CommonItemModel itemModel1,
      @required CommonItemModel itemModel2}) {
    return Column(children: <Widget>[
      Expanded(
          child: _itemWidget(
              context: context, itemModel: itemModel1, isTop: true)),
      Expanded(child: _itemWidget(context: context, itemModel: itemModel2))
    ]);
  }

  Widget _itemWidget(
      {@required BuildContext context,
      @required CommonItemModel itemModel,
      bool isTop = false}) {
    BorderSide borderSide = BorderSide(color: Colors.white, width: 1.0);
    Border border = Border(
        left: borderSide, bottom: (isTop ? borderSide : BorderSide.none));
    return GestureDetector(
        onTap: () =>
            Navigator.of(context).pushNamed('/webView', arguments: itemModel),
        child: Container(
            decoration: BoxDecoration(border: border),
            child: Center(
                child: Text(itemModel.title,
                    style: TextStyle(color: Colors.white, fontSize: 14)))));
  }
}
