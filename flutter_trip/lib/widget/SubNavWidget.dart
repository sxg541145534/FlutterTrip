import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_trip/Model/CommonItemModel.dart';

class SubNavWidget extends StatelessWidget {
  final List<CommonItemModel> subNavList;
  SubNavWidget({@required this.subNavList});

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 2.0,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
      margin: EdgeInsets.fromLTRB(4, 6, 4, 0),
      child: Padding(
          padding: EdgeInsets.only(top: 5.0, bottom: 5.0),
          child: Wrap(
            children: _subItems(context: context),
            runAlignment: WrapAlignment.center,
            runSpacing: 10.0,
          )),
    );
  }

  List<Widget> _subItems({@required BuildContext context}) {
    if (subNavList == null) return [];
    List<Widget> list = List<Widget>();
    int columns = (subNavList.length / 2 + 0.5).toInt();
    int width = ((MediaQuery.of(context).size.width - 8) / columns).floor();
    subNavList.forEach((CommonItemModel itemModel) {
      list.add(
          SizedBox(width: width.toDouble(), child: _item(context, itemModel)));
    });
    return list;
  }

  Widget _item(BuildContext context, CommonItemModel itemModel) {
    return GestureDetector(
        onTap: () =>
            Navigator.of(context).pushNamed('/webView', arguments: itemModel),
        child: Column(children: <Widget>[
          Image.network(
            itemModel.icon,
            width: 30.0,
            height: 30.0,
          ),
          SizedBox(height: 5.0),
          Text(itemModel.title, style: TextStyle(fontSize: 12.0))
        ]));
  }
}
