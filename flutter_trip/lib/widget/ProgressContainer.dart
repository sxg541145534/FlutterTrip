import 'package:flutter/material.dart';

class ProgressContainer extends StatelessWidget {
  final bool isLoading;
  final Widget child;
  ProgressContainer({@required this.isLoading, @required this.child});

  @override
  Widget build(BuildContext context) {
    return isLoading ? _loadingView : child;
  }

  Widget get _loadingView {
    return Center(
        child: CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation<Color>(Colors.blue)));
  }
}
