import 'package:flutter/material.dart';
import 'package:flutter_trip/Model/CommonItemModel.dart';
import 'package:flutter_trip/Model/SalesBoxModel.dart';

class SalesBoxWidget extends StatelessWidget {
  final SalesBoxModel salesBox;
  SalesBoxWidget({@required this.salesBox});

  @override
  Widget build(BuildContext context) {
    return Card(
        elevation: 1.0,
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
        margin: EdgeInsets.fromLTRB(4, 6, 4, 0),
        child: PhysicalModel(
            color: Colors.transparent,
            clipBehavior: Clip.antiAlias,
            borderRadius: BorderRadius.circular(10.0),
            child: Padding(
                padding: EdgeInsets.fromLTRB(10.0, 0, 10.0, 0),
                child: Column(children: _salesBoxList(context: context)))));
  }

  List<Widget> _salesBoxList({@required BuildContext context}) {
    if (salesBox == null) return [];
    List<Widget> list = List<Widget>();

    Widget widget = Container(
        height: 44.0,
        color: Colors.white,
        child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Image.network(salesBox.icon, fit: BoxFit.cover, height: 20.0),
              GestureDetector(
                  onTap: () {
                    Navigator.of(context).pushNamed('/webView',
                        arguments: CommonItemModel(
                            url: salesBox.moreUrl, title: '更多活动'));
                  },
                  child: Container(
                      padding: EdgeInsets.only(left: 10.0, right: 10.0),
                      decoration: BoxDecoration(
                          color: Colors.pink,
                          borderRadius: BorderRadius.circular(10.0)),
                      child: Text('获取更多福利>',
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 14.0,
                              letterSpacing: 2.0))))
            ]));
    list.add(widget);

    list.add(_salesRow(
        context: context,
        itemModel1: salesBox.bigCard1,
        itemModel2: salesBox.bigCard2,
        isBigCard: true));
    list.add(_salesRow(
        context: context,
        itemModel1: salesBox.smallCard1,
        itemModel2: salesBox.smallCard2,
        isBigCard: false));
    list.add(_salesRow(
        context: context,
        itemModel1: salesBox.smallCard3,
        itemModel2: salesBox.smallCard4,
        isBigCard: false));
    return list;
  }

  Widget _salesRow(
      {@required BuildContext context,
      @required CommonItemModel itemModel1,
      @required CommonItemModel itemModel2,
      @required bool isBigCard}) {
    double height = isBigCard ? 140.0 : 70.0;
    return Row(children: <Widget>[
      Expanded(
          child: _salesItem(
              context: context,
              itemModel: itemModel1,
              isFirst: true,
              height: height)),
      Expanded(
          child: _salesItem(
              context: context,
              itemModel: itemModel2,
              isFirst: false,
              height: height))
    ]);
  }

  Widget _salesItem(
      {@required BuildContext context,
      @required CommonItemModel itemModel,
      @required bool isFirst,
      @required double height}) {
    BorderSide borderSide =
        BorderSide(width: 1.0, color: Colors.grey.withAlpha(100));
    return GestureDetector(
        onTap: () {
          Navigator.of(context).pushNamed('/webView', arguments: itemModel);
        },
        child: Container(
          height: height,
          decoration: BoxDecoration(
              border: Border(
                  top: borderSide,
                  right: (isFirst ? borderSide : BorderSide.none))),
          child:
              Image.network(itemModel.icon, alignment: Alignment.bottomCenter),
        ));
  }
}
