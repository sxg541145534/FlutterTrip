import 'package:flutter/material.dart';
import 'package:flutter_trip/Model/CommonItemModel.dart';
import 'package:flutter_trip/Model/TripItemModel.dart';

class TripCardWidget extends StatefulWidget {
  final TripItemModel itemModel;
  TripCardWidget({@required this.itemModel});

  @override
  _TripCardWidgetState createState() => _TripCardWidgetState();
}

class _TripCardWidgetState extends State<TripCardWidget> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          Navigator.of(context).pushNamed('/webView',
              arguments: CommonItemModel(
                  url: widget.itemModel.article.urls.first.h5Url, title: '详情'));
        },
        child: Card(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5.0)),
            child: PhysicalModel(
                color: Colors.white,
                clipBehavior: Clip.antiAlias,
                borderRadius: BorderRadius.circular(5.0),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      _imageWidget,
                      Padding(
                          padding: EdgeInsets.all(4),
                          child: Text(
                            widget.itemModel.article.articleTitle,
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(fontSize: 13),
                          )),
                      _bottomWidget
                    ]))));
  }

  Widget get _imageWidget {
    return Stack(children: <Widget>[
      Image.network(widget.itemModel.article?.images[0]?.originalUrl ?? ''),
      Positioned(
          bottom: 5.0,
          left: 5.0,
          child: Container(
              padding: EdgeInsets.fromLTRB(3, 2, 3, 2),
              decoration: BoxDecoration(
                  color: Colors.black54,
                  borderRadius: BorderRadius.circular(10)),
              child: Row(children: <Widget>[
                Icon(
                  Icons.location_on,
                  color: Colors.white,
                  size: 10.0,
                ),
                LimitedBox(
                    maxWidth: 130.0,
                    child: Text(
                      widget.itemModel?.article?.articleTitle ?? '',
                      style: TextStyle(color: Colors.white, fontSize: 10),
                      overflow: TextOverflow.ellipsis,
                    ))
              ])))
    ]);
  }

  Widget get _bottomWidget {
    return Padding(
        padding: EdgeInsets.fromLTRB(4, 10, 4, 10),
        child: Row(children: <Widget>[
          ClipOval(
              child: Image.network(
            widget.itemModel.article.author.originalUrl,
            width: 30.0,
            height: 30.0,
          )),
          Expanded(
              child: Padding(
                  padding: EdgeInsets.only(left: 4.0, right: 4.0),
                  child: Text((widget.itemModel.article.author.nickName),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(fontSize: 12)))),
          Icon(
            Icons.thumb_up,
            color: Colors.grey,
            size: 20.0,
          ),
          Text((widget.itemModel?.article?.likeCount ?? 0).toString(),
              style: TextStyle(color: Colors.grey, fontSize: 12.0))
        ]));
  }
}
