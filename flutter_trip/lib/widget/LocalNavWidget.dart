import 'package:flutter/material.dart';
import 'package:flutter_trip/Model/CommonItemModel.dart';

class LocalNavWidget extends StatelessWidget {
  final List<CommonItemModel> localNavList;
  LocalNavWidget({@required this.localNavList});

  List<Widget> _getItems(BuildContext context) {
    if (localNavList == null) return [];

    List<Widget> list = List<Widget>();
    localNavList.forEach((CommonItemModel itemModel) {
      Widget widget = GestureDetector(
          onTap: () =>
              Navigator.pushNamed(context, '/webView', arguments: itemModel),
          child: Column(children: <Widget>[
            Image.network(itemModel.icon, width: 36.0, height: 36.0),
            SizedBox(height: 5.0),
            Text(itemModel.title)
          ]));

      list.add(widget);
    });
    return list;
  }

  @override
  Widget build(BuildContext context) {
    return Card(
        margin: EdgeInsets.fromLTRB(4, 6, 4, 0),
        elevation: 2,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        child: Container(
            padding: EdgeInsets.fromLTRB(10, 5, 10, 0),
            decoration: BoxDecoration(
                color: Colors.white, borderRadius: BorderRadius.circular(10)),
            height: 70,
            child: Row(
                children: _getItems(context),
                mainAxisAlignment: MainAxisAlignment.spaceBetween)));
  }
}
