import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:flutter_trip/Model/CommonItemModel.dart';

class BannerListWidget extends StatelessWidget {
  final List<CommonItemModel> bannerList;
  BannerListWidget({@required this.bannerList});

  Widget _getChild() {
    if (bannerList == null) return null;

    return Swiper(
        itemCount: bannerList.length,
        autoplay: true,
        pagination: SwiperPagination(),
        itemBuilder: (context, index) {
          return GestureDetector(
            onTap: () {
              Navigator.of(context).pushNamed('/webView',
                  arguments: CommonItemModel(
                      url: bannerList[index].url, title: '热门推送'));
            },
            child: Image.network(bannerList[index].icon, fit: BoxFit.cover),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Container(height: 160.0, child: _getChild());
  }
}
