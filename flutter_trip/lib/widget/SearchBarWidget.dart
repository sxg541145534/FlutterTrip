import 'package:flutter/material.dart';

class SearchBarWidget extends StatefulWidget {
  /// 是否是首页搜索栏
  final bool isHomeSearchBar;

  /// 占位文本
  final String placeholder;

  /// 是否是活跃状态
  final bool isActive;

  /// 用户所处城市
  final String city;

  /// 搜索关键词
  final String defaultKeyword;

  /// 文本改变监听方法
  final void Function(String) onChanged;

  /// 是否显示返回按钮
  final bool isShowBackButton;

  /// 返回按钮点击事件
  final void Function() backButtonClick;

  /// 点击首页搜索框事件
  final void Function() tapHomeSearchBar;

  /// 点击城市按钮事件
  final void Function() cityButtonClick;

  /// 点击语音按钮事件
  final void Function() microphoneButtonClick;

  SearchBarWidget(
      {@required this.isHomeSearchBar,
      this.isActive = false,
      this.city,
      this.defaultKeyword,
      this.placeholder = '网红打卡地 景点 酒店 美食',
      this.isShowBackButton = false,
      this.onChanged,
      this.tapHomeSearchBar,
      this.backButtonClick,
      this.cityButtonClick,
      this.microphoneButtonClick});

  @override
  _SearchBarWidgetState createState() => _SearchBarWidgetState();
}

class _SearchBarWidgetState extends State<SearchBarWidget> {
  final TextEditingController _editingController = TextEditingController();
  bool _isShowClear = false;

  @override
  void initState() {
    super.initState();

    if (widget.defaultKeyword != null && widget.defaultKeyword.isNotEmpty) {
      _editingController.text = widget.defaultKeyword;
    }

    _editingController.addListener(() {
      setState(() {
        _isShowClear = _editingController.text.length > 0;
      });

      if (widget.onChanged != null) {
        widget.onChanged(_editingController.text);
      }
    });
  }

  @override
  void dispose() {
    _editingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.only(top: 7.0, bottom: 7.0),
        child: Container(
            height: 30.0,
            child: widget.isHomeSearchBar
                ? _homeSearchWidget
                : _normalSearchWidget));
  }

  /// 普通状态搜索栏
  Widget get _normalSearchWidget {
    return Row(children: <Widget>[
      widget.isShowBackButton
          ? _backButton
          : Padding(padding: EdgeInsets.only(left: 15)),
      Expanded(child: _searchTextField),
      _searchButton
    ]);
  }

  Widget get _backButton {
    return Padding(
        padding: EdgeInsets.only(left: 10.0, right: 10.0),
        child: _tapGestureWidget(
            child: Icon(
              Icons.arrow_back_ios,
              color: Colors.grey,
            ),
            onTap: widget.backButtonClick));
  }

  Widget get _searchTextField {
    return Container(
        height: 30.0,
        decoration: BoxDecoration(
            color: Color.fromARGB(255, 237, 237, 237),
            borderRadius: BorderRadius.circular(5.0)),
        child: Row(children: <Widget>[
          Padding(
              padding: EdgeInsets.only(left: 5.0, right: 5.0),
              child: _tapGestureWidget(
                  child: Icon(Icons.search, size: 20.0, color: Colors.grey),
                  onTap: () {})),
          Expanded(
              child: widget.isHomeSearchBar
                  ? _tapGestureWidget(
                      child: Text(
                        widget.placeholder,
                        style: TextStyle(color: Colors.grey, fontSize: 15.0),
                      ),
                      onTap: widget.tapHomeSearchBar)
                  : TextField(
                      autofocus: true,
                      controller: _editingController,
                      style: TextStyle(fontSize: 15.0),
                      decoration: InputDecoration(
                          contentPadding: EdgeInsets.zero,
                          border: InputBorder.none,
                          hintText: widget.placeholder))),
          Padding(
              padding: EdgeInsets.only(left: 5.0, right: 5.0),
              child: (_isShowClear
                  ? _tapGestureWidget(
                      child: Icon(Icons.clear, color: Colors.grey, size: 20.0),
                      onTap: _clearText)
                  : _tapGestureWidget(
                      child: Icon(Icons.mic, color: Colors.blue, size: 20.0),
                      onTap: widget.microphoneButtonClick)))
        ]));
  }

  Widget get _searchButton {
    return Padding(
        padding: EdgeInsets.only(left: 10.0, right: 10.0),
        child: _tapGestureWidget(
            child:
                Text('搜索', style: TextStyle(color: Colors.blue, fontSize: 20)),
            onTap: () {}));
  }

  Widget get _homeSearchWidget {
    return Row(children: <Widget>[
      _cityButton,
      Expanded(child: _searchTextField),
      _messageButton
    ]);
  }

  Widget get _cityButton {
    return Padding(
      padding: EdgeInsets.only(left: 7.0, right: 5.0),
      child: _tapGestureWidget(
          child: Row(children: <Widget>[
            Text(widget.city,
                style: TextStyle(fontSize: 15.0, color: _homeSearchBarColor)),
            Icon(Icons.arrow_drop_down, color: _homeSearchBarColor)
          ]),
          onTap: widget.cityButtonClick),
    );
  }

  Widget get _messageButton {
    return Padding(
        padding: EdgeInsets.only(left: 7.0, right: 5.0),
        child: _tapGestureWidget(
            child: Icon(Icons.message, color: _homeSearchBarColor),
            onTap: null));
  }

  Widget _tapGestureWidget(
      {@required Widget child, @required VoidCallback onTap}) {
    return GestureDetector(
      child: child,
      onTap: onTap,
    );
  }

  void _clearText() {
    _editingController.clear();
  }

  Color get _homeSearchBarColor {
    return widget.isActive ? Colors.grey : Colors.white;
  }
}
