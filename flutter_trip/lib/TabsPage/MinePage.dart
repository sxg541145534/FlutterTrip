import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class MinePage extends StatefulWidget {
  @override
  _MinePageState createState() => _MinePageState();
}

class _MinePageState extends State<MinePage>
    with AutomaticKeepAliveClientMixin {
  /// 我的界面 h5接口
  final String _mineURL = 'https://m.ctrip.com/webapp/myctrip/';

  /// 控制器
  WebViewController _webViewController;

  /// 是否正在加载
  bool _isLoading = true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    EdgeInsets padding = MediaQuery.of(context).padding;
    return Padding(
        padding: EdgeInsets.only(top: padding.top),
        child: Stack(children: <Widget>[
          WebView(
              initialUrl: _mineURL,
              javascriptMode: JavascriptMode.unrestricted,
              onWebViewCreated: (viewController) =>
                  _webViewController = viewController,
              onPageFinished: (url) {
                setState(() {
                  _isLoading = false;
                });
              }),
          Visibility(
              visible: _isLoading,
              child: Container(
                  color: Colors.white,
                  child: Center(
                      child: CircularProgressIndicator())))
        ]));
  }

  @override
  bool get wantKeepAlive => true;
}
