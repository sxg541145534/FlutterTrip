import 'package:flutter/material.dart';
import 'package:flutter_trip/DAO/TripDAO.dart';
import 'package:flutter_trip/Model/AdvertiseModel.dart';

class SplashPage extends StatefulWidget {
  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  /// 广告图片url
  String _url;
  @override
  void initState() {
    super.initState();
    _loadAdvertise();

    Future.delayed(Duration(seconds: 4),
        () => Navigator.of(context).pushReplacementNamed('/tabs'));
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        color: Colors.white,
        child: Center(
          child: _url != null
              ? Image.network(_url, fit: BoxFit.cover)
              : Container(),
        ));
  }

  /// 加载广告数据
  void _loadAdvertise() async {
    AdvertiseModel advertiseModel = await TripDAO.loadAdvertiseData();
    if (advertiseModel != null) {
      setState(() {
        _url = advertiseModel.url;
      });
    }
  }
}
