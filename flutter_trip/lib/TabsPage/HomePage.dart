import 'package:flutter/material.dart';
import 'package:flutter_trip/DAO/TripDAO.dart';
import 'package:flutter_trip/Model/GridNavModel.dart';
import 'package:flutter_trip/Model/HomeModel.dart';
import 'package:flutter_trip/Model/CommonItemModel.dart';
import 'package:flutter_trip/Model/SalesBoxModel.dart';
import 'package:flutter_trip/Widget/GridNavWidget.dart';
import 'package:flutter_trip/Widget/NavBarWidget.dart';
import 'package:flutter_trip/Widget/ProgressContainer.dart';
import 'package:flutter_trip/Widget/SalesBoxWidget.dart';
import 'package:flutter_trip/Widget/BannerListWidget.dart';
import 'package:flutter_trip/Widget/LocalNavWidget.dart';
import 'package:flutter_trip/Widget/SubNavWidget.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with AutomaticKeepAliveClientMixin {
  /// 广告轮播数组
  List<CommonItemModel> _bannerList;

  /// 导航列表
  List<CommonItemModel> _localNavList;

  /// 卡片模型
  GridNavModel _gridNav;

  /// 子菜单列表
  List<CommonItemModel> _subNavList;

  /// 活动模型
  SalesBoxModel _salesBox;

  /// 滚动控制器
  final ScrollController _scrollController = ScrollController();

  /// 导航栏透明度
  double _appBarAlpha = 0.0;

  /// 是否正在加载
  bool _isLoading = true;

  @override
  bool get wantKeepAlive => true; // 保存页面状态

  @override
  void initState() {
    super.initState();
    _loadData();

    // 监听list滚动
    _scrollController.addListener(() {
      setState(() {
        _appBarAlpha = _scrollController.offset / 100.0;
        if (_appBarAlpha > 1.0) {
          _appBarAlpha = 1.0;
        } else if (_appBarAlpha < 0) {
          _appBarAlpha = 0.0;
        }
      });
    });
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    Widget widget = Stack(children: <Widget>[
      MediaQuery.removePadding(
        context: context,
        removeTop: true,
        child: _listView,
      ),
      NavBarWidget(alpha: _appBarAlpha)
    ]);

    return Scaffold(
        body: ProgressContainer(isLoading: _isLoading, child: widget));
  }

  void _loadData() async {
    HomeModel homeModel = await TripDAO.loadHomeData();
    setState(() {
      _bannerList = homeModel?.bannerList;
      _localNavList = homeModel?.localNavList;
      _gridNav = homeModel?.gridNav;
      _subNavList = homeModel?.subNavList;
      _salesBox = homeModel?.salesBox;
      _isLoading = false;
    });
  }

  Widget get _listView {
    return ListView(controller: _scrollController, children: <Widget>[
      BannerListWidget(bannerList: _bannerList),
      LocalNavWidget(localNavList: _localNavList),
      GridNavWidget(gridNav: _gridNav),
      SubNavWidget(subNavList: _subNavList),
      SalesBoxWidget(salesBox: _salesBox),
      Container(
          margin: EdgeInsets.only(top: 10.0, bottom: 10.0),
          child: Center(
              child: Text('本App由邵兴国完成', style: TextStyle(color: Colors.grey))))
    ]);
  }
}
