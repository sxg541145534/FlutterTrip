import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:flutter_trip/DAO/TripDAO.dart';
import 'package:flutter_trip/Model/TripItemModel.dart';
import 'package:flutter_trip/Widget/ProgressContainer.dart';
import 'package:flutter_trip/Widget/TripCardWidget.dart';

class TripChannelPage extends StatefulWidget {
  final Map<String, dynamic> params;
  final String groupChannelCode;
  TripChannelPage({this.params, this.groupChannelCode});

  @override
  _TripChannelPageState createState() => _TripChannelPageState();
}

class _TripChannelPageState extends State<TripChannelPage>
    with AutomaticKeepAliveClientMixin {
  /// 当前页码
  int _currentIndex = 0;

  /// 卡片数据模型数组
  List<TripItemModel> _resultList;

  /// 是否正在加载中
  bool _isLoading = true;

  final ScrollController _scrollController = ScrollController();

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    super.initState();
    _refreshData();
    _scrollController.addListener(() {
      if (_scrollController.offset == _scrollController.position.maxScrollExtent && _scrollController.offset > 0) {
        _loadMoreData();
      }
    });
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return ProgressContainer(
        isLoading: _isLoading,
        child: RefreshIndicator(
          onRefresh: _refreshData,
          child: StaggeredGridView.countBuilder(
            controller: _scrollController,
              crossAxisCount: 2, // 一行多少列
              itemCount: _resultList?.length ?? 0,
              itemBuilder: (BuildContext context, int index) =>
              _resultList[index] != null
                  ? TripCardWidget(itemModel: _resultList[index])
                  : null,
              staggeredTileBuilder: (int index) => StaggeredTile.fit(1)),
        )); //占多少列

    /*
    return StaggeredGridView.countBuilder(
      crossAxisCount: 4, // 一共占4列
      itemCount: 8,
      itemBuilder: (BuildContext context, int index) => new Container(
          color: Colors.green,
          child: new Center(
            child: new CircleAvatar(
              backgroundColor: Colors.white,
              child: new Text('$index'),
            ),
          )),
      staggeredTileBuilder: (int index) =>
      new StaggeredTile.count(2, index.isEven ? 3 : 1), 参数一 显示4/2=2列 参数二 奇数和偶数列的高度比 这里为偶数列是奇数列高度三倍
      mainAxisSpacing: 10.0,
      crossAxisSpacing: 4.0,
    );*/
  }

  /// 加载更多数据
  void _loadMoreData() async {
    _currentIndex++;
    TripItemsModel itemsModel = await TripDAO.loadTripItemData(
        groupChannelCode: widget.groupChannelCode,
        pageIndex: _currentIndex,
        parameters: widget.params);
    setState(() {
      _isLoading = false;
      if (_resultList != null) {
        _resultList.addAll(itemsModel.resultList);
      } else {
        _resultList = itemsModel.resultList;
      }
    });
  }

  /// 刷新数据
  Future<void> _refreshData() async {
    _currentIndex = 1;
    TripItemsModel itemsModel = await TripDAO.loadTripItemData(
        groupChannelCode: widget.groupChannelCode,
        pageIndex: _currentIndex,
        parameters: widget.params);
    setState(() {
      _isLoading = false;
      if (itemsModel.resultList != null) {
        _resultList = itemsModel.resultList;
      }
    });
  }
}
