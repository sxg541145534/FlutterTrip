import 'package:flutter/material.dart';
import 'package:flutter_trip/TabsPage/SearchPage.dart';
import 'package:flutter_trip/Tool/ASRManager.dart';
import 'package:toast/toast.dart';

/// 语音按钮尺寸
final double speechButtonSize = 80.0;

class SpeakSearchPage extends StatefulWidget {
  @override
  _SpeakSearchPageState createState() => _SpeakSearchPageState();
}

class _SpeakSearchPageState extends State with TickerProviderStateMixin {
  AnimationController _animationController;
  Animation<double> _animation;
  /// 语音提示文字
  String _tipsText = '长按说话';

  @override
  void initState() {
    super.initState();

    _animationController =
        AnimationController(vsync: this, duration: Duration(seconds: 1));
    _animation = CurvedAnimation(parent: _animationController, curve: Curves.easeIn);
    _animationController.addStatusListener((AnimationStatus status) {
      if (status == AnimationStatus.completed) {
        _animationController.reverse();
      } else if (status == AnimationStatus.dismissed) {
        _animationController.forward();
      }
    });
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            color: Colors.white,
            child: Center(
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[_topItemWidget, _bottomItemWidget]))));
  }

  Widget get _topItemWidget {
    return Padding(
        padding: EdgeInsets.only(top: 44.0),
        child: Column(children: <Widget>[
          Text('你可以这样说', style: TextStyle(color: Colors.orange, fontSize: 25.0)),
          SizedBox(height: 30.0),
          Text('北京故宫\n上海迪士尼\n西安兵马俑',
              textAlign: TextAlign.center,
              style:
                  TextStyle(color: Colors.grey, letterSpacing: 2, fontSize: 16.0))
        ]));
  }

  Widget get _bottomItemWidget {
    return Container(
        margin: EdgeInsets.only(bottom: 30.0),
        height: speechButtonSize + 30,
        color: Colors.white,
        child: Stack(children: <Widget>[
          Align(
              alignment: Alignment(0.9, 1),
              child: IconButton(
                  icon: Icon(
                    Icons.close,
                    color: Colors.grey,
                    size: 40,
                  ),
                  onPressed: () => Navigator.of(context).pop())),
          Align(
              alignment: Alignment(0, 1),
              child: GestureDetector(
                  onTapDown: (TapDownDetails details) => _startSpeech(),
                  onTapUp: (TapUpDetails details) => _finishedSpeech(),
                  onTapCancel: _cancelSpeech,
                  child: SpeechAnimatedWidget(animation: _animation))),
          Align(
              alignment: Alignment(0, -1),
              child: Text(_tipsText, style: TextStyle(color: Colors.blueAccent)))
        ]));
  }

  /// 开始语音
  void _startSpeech() async {
    setState(() {
      _tipsText = '正在识别中…';
    });
    _animationController.forward();

    try {
      String text = await ASRManager.start();
      Navigator.of(context).pop();
      Navigator.of(context).push(MaterialPageRoute(builder: (context) => SearchPage(defaultKeyword: text,isShowBackButton: true,)));
    } catch (error,stack) {
      Toast.show('语音识别失败', context);
    }
  }

  /// 结束语音
  void _finishedSpeech() {
    setState(() {
      _tipsText = '长按说话';
    });

    _animationController.reset();
    _animationController.stop();
    ASRManager.stop();
  }

  /// 取消语音
  void _cancelSpeech() {
    setState(() {
      _tipsText = '长按说话';
    });

    _animationController.reset();
    _animationController.stop();

    ASRManager.cancel();
  }
}

class SpeechAnimatedWidget extends AnimatedWidget {
  /// 透明度动画
  final Tween<double> _opacityTween = Tween<double>(begin: 1, end: 0.3);
  /// 缩放动画
  final Tween<double> _scaleTween =
      Tween<double>(begin: speechButtonSize, end: speechButtonSize * 0.6);

  SpeechAnimatedWidget({@required Animation<double> animation})
      : super(listenable: animation);

  @override
  Widget build(BuildContext context) {
    return Opacity(
        opacity: _opacityTween.evaluate(listenable),
        child: Container(
            child: Icon(Icons.mic, color: Colors.white, size: 30.0),
            width: _scaleTween.evaluate(listenable),
            height: _scaleTween.evaluate(listenable),
            decoration: BoxDecoration(
                color: Colors.blueAccent,
                borderRadius: BorderRadius.circular(speechButtonSize / 2))));
  }
}
