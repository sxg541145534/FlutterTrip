import 'package:flutter/material.dart';
import 'package:flutter_trip/TabsPage/HomePage.dart';
import 'package:flutter_trip/TabsPage/MinePage.dart';
import 'package:flutter_trip/TabsPage/SearchPage.dart';
import 'package:flutter_trip/TabsPage/TripPage.dart';

class TabsPage extends StatefulWidget {
  @override
  _TabsPageState createState() => _TabsPageState();
}

class _TabsPageState extends State<TabsPage> {
  int _currentIndex = 0;
  final PageController _pageController = PageController(initialPage: 0);

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        bottomNavigationBar: BottomNavigationBar(
            currentIndex: _currentIndex,
            type: BottomNavigationBarType.fixed,
            onTap: (index) {
              _pageController.jumpToPage(index);
              setState(() {
                _currentIndex = index;
              });
            },
            items: [
              BottomNavigationBarItem(
                  title: Text('首页'), icon: Icon(Icons.home)),
              BottomNavigationBarItem(
                  title: Text('搜索'), icon: Icon(Icons.search)),
              BottomNavigationBarItem(
                  title: Text('旅拍'), icon: Icon(Icons.photo_camera)),
              BottomNavigationBarItem(
                  title: Text('我的'), icon: Icon(Icons.account_circle)),
            ]),
        body: PageView(
            controller: _pageController,
            physics: NeverScrollableScrollPhysics(),
            children: <Widget>[
              HomePage(),
              SearchPage(),
              TripPage(),
              MinePage()
            ]));
  }
}
