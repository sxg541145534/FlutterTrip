import 'package:flutter/material.dart';
import 'package:flutter_trip/Routes.dart';
import 'package:flutter_trip/TabsPage/SplashPage.dart';
import 'package:flutter_trip/TabsPage/TabsPage.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: ThemeData(
            primaryColor: Colors.blue,
            primaryColorBrightness: Brightness.light),
        onGenerateRoute: routeGenerator,
        debugShowCheckedModeBanner: false,
        title: 'FlutterTrip',
        home: SplashPage());
  }
}
