import 'package:flutter/services.dart';

class ASRManager 
{
  static const MethodChannel _channel = MethodChannel('speechPlugin') ;

  static Future<String> start() async {
    String text = await _channel.invokeMethod('start');
    return text;
  }

  static Future<void> stop() async {
    return await _channel.invokeMethod('stop');
  }

  static Future<void> cancel() async {
    return await _channel.invokeMethod('cancel');
  }
}