//
//  VoiceManager.m
//  demo
//
//  Created by monkey on 2019/11/3.
//  Copyright © 2019 itcast. All rights reserved.
//

#import "XGSpeechManager.h"

#import "BDSASRDefines.h"
#import "BDSASRParameters.h"
#import "BDSEventManager.h"

const NSString* APP_ID = @"17691033";
const NSString* API_KEY = @"QBmEdillEDWAtHGFLXg3dU6t";
const NSString* SECRET_KEY = @"c3TBQDyfCOQGtLkUeY5DhoZ6vlCgMoyh";


@interface XGSpeechManager () <BDSClientASRDelegate>
{
    BDSEventManager *_asrEventManager;
    /// 完成回调
    void (^_completion)(NSString *_Nullable,NSError *_Nullable);
}

@end

@implementation XGSpeechManager

#pragma mark - 语音识别相关

- (void)start
{
    [_asrEventManager setParameter:@(NO) forKey:BDS_ASR_ENABLE_LONG_SPEECH];
    [_asrEventManager setParameter:@(NO) forKey:BDS_ASR_NEED_CACHE_AUDIO];
    [_asrEventManager setParameter:nil forKey:BDS_ASR_AUDIO_FILE_PATH];
    [_asrEventManager setParameter:nil forKey:BDS_ASR_AUDIO_INPUT_STREAM];
    [_asrEventManager setDelegate:self];
    [_asrEventManager setParameter:@"" forKey:BDS_ASR_OFFLINE_ENGINE_TRIGGERED_WAKEUP_WORD];
    [_asrEventManager sendCommand:BDS_ASR_CMD_START];
}

- (void)stop
{
    [_asrEventManager sendCommand:BDS_ASR_CMD_STOP];
}

- (void)cancel
{
    [_asrEventManager sendCommand:BDS_ASR_CMD_CANCEL];
}

#pragma mark - 构造方法

- (instancetype)initWithCompletion:(void (^)(NSString * _Nullable, NSError * _Nullable))completion
{
    if (self = [super init]) {
        _completion = completion;
        _asrEventManager = [BDSEventManager createEventManagerWithName:BDS_ASR_NAME];
        [self configVoiceRecognitionClient];
    }
    
    return self;
}

#pragma mark - BDSClientASRDelegate

- (void)VoiceRecognitionClientWorkStatus:(int)workStatus obj:(id)aObj
{
    switch (workStatus) {
        case EVoiceRecognitionClientWorkStatusStart:
            // NSLog(@"start");
            break;
        case EVoiceRecognitionClientWorkStatusFinish:
            if (aObj != nil && [aObj isKindOfClass:[NSDictionary class]]) {
                NSArray *results = aObj[@"results_recognition"];
                if (_completion) {
                    _completion(results.firstObject,nil);
                }
            }
            break;
        case EVoiceRecognitionClientWorkStatusCancel:
            // NSLog(@"cancel");
            break;
        case EVoiceRecognitionClientWorkStatusError:
            if (aObj != nil && [aObj isKindOfClass:[NSError class]]) {
                if (_completion) {
                    _completion(nil,aObj);
                }
            }
            break;
        default:
            break;
    }
}

#pragma mark - SDK初始化配置

- (void)configVoiceRecognitionClient
{
    //设置DEBUG_LOG的级别
    [_asrEventManager setParameter:@(EVRDebugLogLevelTrace) forKey:BDS_ASR_DEBUG_LOG_LEVEL];
    //配置API_KEY 和 SECRET_KEY 和 APP_ID
    [_asrEventManager setParameter:@[API_KEY, SECRET_KEY] forKey:BDS_ASR_API_SECRET_KEYS];
    [_asrEventManager setParameter:APP_ID forKey:BDS_ASR_OFFLINE_APP_CODE];
    //配置端点检测（二选一）
    [self configModelVAD];
}

- (void)configModelVAD
{
    NSString *modelVAD_filepath = [[NSBundle mainBundle] pathForResource:@"bds_easr_basic_model" ofType:@"dat"];
    [_asrEventManager setParameter:modelVAD_filepath forKey:BDS_ASR_MODEL_VAD_DAT_FILE];
    [_asrEventManager setParameter:@(YES) forKey:BDS_ASR_ENABLE_MODEL_VAD];
}

@end
