//
//  XGSpeechPlugin.h
//  Runner
//
//  Created by monkey on 2019/11/4.
//  Copyright © 2019 The Chromium Authors. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Flutter/Flutter.h>

@interface XGSpeechPlugin : NSObject <FlutterPlugin>

@end

