//
//  VoiceManager.h
//  demo
//
//  Created by monkey on 2019/11/3.
//  Copyright © 2019 itcast. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface XGSpeechManager : NSObject

/// 开始
- (void)start;
/// 停止
- (void)stop;
/// 取消
- (void)cancel;


/// 指定构造函数
/// @param completion 完成回调
- (_Nonnull instancetype)initWithCompletion:(void (^ _Nonnull)(NSString *_Nullable,NSError *_Nullable))completion;
@end

