import UIKit
import Flutter

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) -> Bool {
    
    GeneratedPluginRegistrant.register(with: self)

    // 注册自己的plugin
    XGSpeechPlugin.register(with: registrar(forPlugin: "XGSpeechPlugin"))
    return super.application(application, didFinishLaunchingWithOptions: launchOptions)
  }
}
